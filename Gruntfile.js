module.exports = function (grunt) {
    require('time-grunt')(grunt);
    require('load-grunt-tasks')(grunt);
    // 1. All configuration goes here
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        secret: grunt.file.readJSON('secret.json'),
        connect: {
            server: {
                options: {
                    livereload: true,
                    base: 'dest/',
                    port: 9009
                }
            }
        },
        clean: {
            main: {
                src: ['dest']
            }
        },
        autoprefixer: {

            options: {
                browsers: ['last 2 versions', 'ie 9']
            },

            // prefix the specified file
            single_file: {
                options: {
                    // Target-specific options go here.
                },
                src: 'assets/css/main-dev.css',
                dest: 'assets/css/main-dev.css'
            },
        },
        watch: {

            styles: {
                // Which files to watch (all .less files recursively in the less directory)
                files: ['less/*.less', 'css/less/*.less'],
                tasks: ['less:development', 'autoprefixer', 'copy'],
                options: {
                    nospawn: true,
                    atBegin: true
                }
            },
            html: {
                files: ['_layouts/*.html',
                    '_includes/**',
                    '_posts/*.markdown',
                    '_config.yml',
                    'pages/**',
                    'index.html',
                    '404.html'],
                tasks: ['shell:jekyllBuild'],
                options: {
                    spawn: true,
                    interrupt: true,
                    atBegin: true,
                    livereload: true
                }
            },
            script: {
                // Which files to watch (all .less files recursively in the less directory)
                // files: ['js/custom/main.js'],
                files: ['js/*/main.js', 'js/*.js'],
                tasks: ['concat', 'copy'],
                options: {
                    nospawn: true,
                    atBegin: true
                }
            }
        },
        shell: {
            jekyllBuild: {
                command: 'jekyll build'
            },
            jekyllServe: {
                command: 'jekyll serve'
            }
        },
        concat: {
            dist_a: {
                src: [
                    'bower_components/jquery/dist/jquery.js', // All JS in the libs folder
                    'bower_components/bootstrap/dist/js/bootstrap.min.js',  // This specific file
                ],
                dest: 'assets/js/production.js',
            },
            dist_b: {
                src: ['assets/js/main.js',],
                dest: 'assets/js/custom.js',
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true, cwd: 'bower_components/fontawesome/fonts/', src: ['**'], dest: 'assets/fonts/'},
                ]
            },
            assets: {
                files: [
                    // includes files within path
                    {expand: true, cwd: 'assets/', src: ['**'], dest: 'dest/assets/'},
                ]
            }
        },

        uglify: {
            build: {
                src: 'assets/js/production.js',
                dest: 'assets/js/production.min.js'
            },
            custom: {
                src: 'assets/js/custom.js',
                dest: 'assets/js/custom.min.js'
            }
        },

        less: {
            development: {
                options: {
                    paths: ["assets/css"]
                },
                files: {
                    "assets/css/main-dev.css": "less/main.less"
                }
            },
            production: {
                options: {
                    paths: ["assets/css"],
                    cleancss: true
                },
                files: {
                    "assets/css/main-dev.css": "less/main.less"
                }
            }
        },
        environments: {
            options: {
                local_path: './dest',
                current_symlink: 'current',
                deploy_path: '/nfs/vsp/dds.nl/e/enning/public_html/photo-o-matic'
            },
            staging: {
                options: {
                    host: '<%= secret.staging.host %>',
                    username: '<%= secret.staging.username %>',
                    password: '<%= secret.staging.password %>',
                    port: '<%= secret.staging.port %>',
                    debug: true,
                    number_of_releases: '3'
                }
            },
            production: {
                options: {
                    host: '<%= secret.production.host %>',
                    username: '<%= secret.production.username %>',
                    password: '<%= secret.production.password %>',
                    port: '<%= secret.production.port %>',
                    number_of_releases: '5'
                }
            }
        }

    });

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ['clean','concat', 'uglify', 'less:development', 'autoprefixer', 'shell:jekyllBuild','copy']);
    grunt.registerTask('deploy', ['clean','concat', 'uglify', 'less:production', 'autoprefixer', 'shell:jekyllBuild','copy','ssh_deploy:staging']);
    grunt.registerTask('server', ['clean', 'connect:server', 'watch']);

};
